$('.slider-for').each(function(key, item) {

    var sliderIdName = 'slider' + key;
    var sliderNavIdName = 'sliderNav' + key;
  
    this.id = sliderIdName;
    $('.slider-nav')[key].id = sliderNavIdName;
  
    var sliderId = '#' + sliderIdName;
    var sliderNavId = '#' + sliderNavIdName;

            $('slider-1').slick({
            dots: false,
            arrows: true,
            infinite: true,
            speed: 500,
            fade: true,
            cssEase: 'linear'
            });

            $('slider-2').slick({
            dots: false,
            arrows: true,
            infinite: true,
            speed: 500,
            fade: true,
            cssEase: 'linear'
            });

            $('slider-3').slick({
            dots: false,
            arrows: true,
            infinite: true,
            speed: 500,
            fade: true,
            cssEase: 'linear'
            });
