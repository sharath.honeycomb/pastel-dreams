
$(document).ready(function () {
    var didScroll;
    var lastScrollTop = 0;
    var delta = 1;
    var offsetHeight = ($(window).width() < 1015) ? 34 : 50;
	if(screen.width < 1024){
	   var cookieHeight = $(".cookies-msg").length ? $(".cookies-msg").outerHeight(true) :0;
	}else{
		var cookieHeight = 0;
	}

    var header = $("#header header"),
        headerHeight = header.height(),
        windowHeight = $(window).height(),
        menuHeight = windowHeight - stickyHeader,
		topValueforSticky=headerHeight + cookieHeight,
		topValueforHeader=cookieHeight,
		topValueOnScrollforHeader=topValueforHeader-offsetHeight,
        topValueOnScrollforSticky = topValueforSticky - offsetHeight,
        navbarHeight = $(".navbar-brand").outerHeight();
		
    var searchScroll = true;
    var $currentScrollPos;
    $(this).scrollTop(0);
    header.css("top", topValueforHeader);
    $('.minicart-zoom .modal-dialog').css("top", navbarHeight);
    $('body').css("padding-top", topValueforSticky);
    $('.search-box').css("top", topValueforSticky);
    $('.facets-filter-stickyMobile').css("top", topValueforSticky);
    header.css("position", "fixed");
    if ($("body").hasClass("pace-done")) {
        $(window).resize()
    }
	
    var recalculateValues = function () {
		if(screen.width < 1024){
			cookieHeight = $(".cookies-msg").length ? $(".cookies-msg").outerHeight(true) :0;
		}else{
			cookieHeight = 0;
		}
        offsetHeight = ($(window).width() < 1015) ? 34 : 50;
        header = $("#header header");
		topValueforHeader=cookieHeight;
        headerHeight = header.height();
        windowHeight = $(window).height();
        menuHeight = windowHeight - stickyHeader;
		topValueforSticky=headerHeight + cookieHeight;
		topValueOnScrollforHeader=topValueforHeader-offsetHeight;
        topValueOnScrollforSticky = topValueforSticky - offsetHeight;
        navbarHeight = $(".navbar-brand").outerHeight();
        header.css("top", topValueforHeader);
        header.css("position", "fixed");
        var finalTopValue;
        $('body').css("padding-top", topValueforSticky);
        if ($('.collapsible-nav-option[aria-expanded="true"]').length) {
            if ($("#header header").hasClass('stickyLogo')) {
                finalTopValue = topValueOnScrollforSticky;
            } else {
                finalTopValue = topValueforSticky;
            }
            $('.search-box-wrapper').css("top", finalTopValue);
            $('.facets-filter-stickyMobile').css("top", finalTopValue - 2);
            $("#menu").css("height", windowHeight - (finalTopValue));
            if ($(this).width() < 1015 && $(this).width() > 767 && $('#wrap').hasClass("fix-search")) {
                $('.filter-compare.').css("top", finalTopValue);
            }
        }
    }
    $(window).on('resize', function () {
        recalculateValues();
        if ($(this).width() > 1015) {
            if ($("button.navbar-toggle[aria-expanded='true']").length) {
                $("#layoutContainers").removeClass("overlay");
            }
            $('.panel .sublevel').each(function () {
                if ($(this).parent().hasClass('firstClick')) {
                    $("#layoutContainers").addClass("overlay");
                }
            });
        }
        if ($(this).width() < 1015) {
            if ($("button.navbar-toggle[aria-expanded='true']").length) {
                $("#layoutContainers").addClass("overlay");
            }
            if ($("button.navbar-toggle[aria-expanded='false']")) {
                $("#layoutContainers").removeClass("overlay");
            }
            setTimeout(function () {
                $('.facets-filter-stickyMobile').css("top", topValueforSticky - 2);
            });
        }
        if ($(this).width() < 1015 && $(this).width() > 767 && $('#wrap').hasClass("fix-search")) {
            $('.filter-compare').css("top", topValueforSticky);
        }
		if(screen.width < 1024){
			if($("#footerPaymentMethods").css("padding-bottom") > "0px")
				$("#footerPaymentMethods").css("padding-bottom", 0);
		}
    });
    
    function hasScrolled() {
        var st = $(this).scrollTop();
        // Make sure they scroll more than delta
        if (Math.abs(lastScrollTop - st) <= delta)
            return;
        recalculateValues();
        if (st > lastScrollTop && st > headerHeight && searchScroll) {
            // Scroll Down
            header.css("top", topValueOnScrollforHeader);
            header.addClass("stickyLogo");
            header.css("top", topValueOnScrollforHeader);
            $('.select:hidden').stop(true, true).show();
			$('.search-box').css("top", topValueOnScrollforSticky - 3);
            if ($(window).width() < 1015) {
				$('.facets-filter-stickyMobile').css("top", topValueOnScrollforSticky - 2);
            }
            if ($(window).width() < 1015 && $(window).width() > 767 && $('#wrap').hasClass("fix-search")) {
				$('.filter-compare').css("top", topValueOnScrollforSticky);
            }
        } else if (st < lastScrollTop) {
            // Scroll Up
            if (st + $(window).height() < $(document).height()) {
                header.css("top", topValueforHeader);
                header.removeClass("stickyLogo");
				$('.search-box').css("top", topValueforSticky);
                $('.select').stop(true, true).hide();
                if ($(window).width() < 1015) {
					$('.facets-filter-stickyMobile').css("top", topValueforSticky - 2);
                }
            }
            if ($(window).width() < 1015 && $(window).width() > 767 && $('#wrap').hasClass("fix-search")) {
				$('.filter-compare').css("top", topValueforSticky);
            }
        }

        lastScrollTop = st;
    }
    $(window).scroll(function () {
        didScroll = true;
        $currentScrollPos = $(document).scrollTop();
        $(".brand-selector-container").removeClass("in");
        $(".brands-selector").attr("aria-expanded", "false"); // hiding header brand selector on scroll
        $("#currencySelector").removeClass("in");
        $(".currency-selector").attr("aria-expanded", "false"); // hiding currency selector on scroll
        $("header .navbar-nav .panel-heading").unbind('mouseenter mouseleave');
        $("header .sub-menu > div").unbind('mouseenter mouseleave');// hiding submenu if open
        hasScrolled();
    });
	
	$(document).click(function (event) {
        if ($(event.target).closest('.container-fluid').attr('id') != 'currencySelector') {
            closePanel($("#currencySelector"), $("a.currency-selector"));
        }
        if ($(event.target).closest('.container-fluid').attr('id') != 'brandSelector') {
            closePanel($("#brandSelector"), $("a.brands-selector"));
        }
		});
	
	//close currency/brand-selector on click outside
	var closePanel = function (selectorDiv, selectorLink) {
		if (selectorDiv.hasClass('in')) {
			selectorLink.attr("aria-expanded", "false");
            selectorDiv.attr("aria-expanded", "false");
			selectorDiv.removeClass('in');
			}
	}
   
   // on click of desktop hamberger menu
    $('.select').click(function () {
        header.css("top", '');
        header.removeClass("stickyLogo");
        $('.search-box').css("top", headerHeight);
        $('.select').stop(true, true).hide();
    });
    $("#minicart").on('hidden.bs.modal', function () {
        alert('The modal is now hidden.');
    });
    // close search bar on click of links
    $(".suggestion-content a").click(function () {
        $("#searchBox").removeClass('in');
        $(".search a").attr("aria-expanded", "false");
        if ($("#layoutContainers").hasClass("header-background-overlay")) {
            $("#layoutContainers").removeClass("header-background-overlay");
            $("footer").removeClass("header-background-overlay");
            $(".header-overlay").hide();
            $(".search a").attr("aria-expanded", "false");
        }
    });
    $('.sub-menu,.search-box').click(function () {
        event.stopPropagation();
    });
    $("button.navbar-toggle,.center-option .panel-heading a").click(function () {
		if (navigator.userAgent.match(/(iPhone|iPod|iPad)/i)) {
			 searchScroll = true;
		}
        $("#searchBox").removeClass('in');
        $(".search a").attr("aria-expanded", "false");
        setTimeout(function () {
            if ($("#menu").height() > menuHeight) {
                if ($(this).scrollTop() > offsetHeight) {
                    $("#menu").css("height", menuHeight);
                    $("#menu").addClass('overflowY');
                } else {
                    $("#menu").css("height", menuHeight - offsetHeight);
                    $("#menu").addClass('overflowY');
                }
            }
        }, 1000);
    });
	$(document).click(function (event) {
		if (event.target.id != 'searchBox' && event.target.id != 'stPageFrame' && event.target.className != 'input-search-icon' && $(event.target).parents('#searchBox').length <= 0 && event.target.id != 'srch-field' && event.target.id != 'searchIcon' && event.target.id != 'titanSearch' && event.target.id != 'searchBreadCrumb') {
			if (navigator.userAgent.match(/(iPhone|iPod|iPad)/i)) {
				searchScroll = true;
			}
		}
    });
    $(".search a").click(function () {
		if (navigator.userAgent.match(/(iPhone|iPod|iPad)/i)) {
			 if ($("#titanSearch").hasClass('collapsed')){
				 searchScroll = false;
			 }
			 else
				 searchScroll = true;
		}
         if  ($(".search").hasClass('locator')){


            }
        else
        {
        $(".sub-menu,#menu").removeClass('in');
        $("button.navbar-toggle,.center-option .panel-heading a").attr("aria-expanded", "false");
        $("button.navbar-toggle").addClass('collapsed');
        if (!$("#layoutContainers").hasClass("header-background-overlay")) {
            $("#layoutContainers").addClass("header-background-overlay");
            $("footer").addClass("header-background-overlay");
            $(".header-overlay").show();
        }
        }
    });
    $(".no-btn-search a,.no-btn-search").click(function () {
        if ($("#searchBox").hasClass("in")) {
            $("#searchBox").removeClass('in');
            if ($("#layoutContainers").hasClass("header-background-overlay")) {
                $("#layoutContainers").removeClass("header-background-overlay");
                $("footer").removeClass("header-background-overlay");
                $(".header-overlay").hide();
            }
        }
    });

    $(".brands-selector").click(function () {
        $("#currencySelector").removeClass("in");
        $(".currency-selector").attr("aria-expanded", "false");
    }); //accordian effect on brand and currency selector
    $(".currency-selector").click(function () {
        $(".brand-selector-container").removeClass("in");
        $(".brands-selector").attr("aria-expanded", "false");
    }); //accordian effect on brand and currency selector

    //scorllDisabled for menu of mobiles..
    $(".navbar-header button.navbar-toggle").click(function () {
        if ($(this).hasClass("collapsed")) {
            if (!$("#layoutContainers").hasClass("overlay")) {
                $("#layoutContainers").addClass("overlay");
                $('body').css('overflow',  'hidden');
            }
            if (navigator.userAgent.match(/(iPhone|iPod|iPad)/i)) {
                localStorage.cachedScrollPos = $currentScrollPos;
                $('body').css('position', 'fixed');
            }
            if (navigator.userAgent.match(/UCBrowser/i)) {
                localStorage.cachedScrollPos = $currentScrollPos;
                $('body').css('position', 'fixed');
            }
        } else {
            if ($("#layoutContainers").hasClass("overlay")) {
                $("#layoutContainers").removeClass("overlay");
            }
            if (navigator.userAgent.match(/(iPhone|iPod|iPad)/i)) {
                $('body').css('position', 'relative');
                $(window).scrollTop(localStorage.cachedScrollPos);
            }
            $('#content').removeClass('scrollDisabled');
			if ($("body").css("position") == "fixed") {
                    $("body").css({
                        "position": ""
                    });
                }
            if ($("body").css("overflow") == "hidden") {
                $("body").css({
                    "overflow": ""
                });
            }
			$(".megaMenuOverlay").hide();
        }
    });
	var hrefValue = [];

        function isIpad () {
            var ua = navigator.userAgent;
          return /iPad/i.test(ua);
        }
    
        function forTabletTouch() {
            if (typeof window.ontouchstart !== 'undefined' && $(window).width() > 1023 && $(window).width() <= 1280 && hrefValue.length === 0 && window.innerHeight < window.innerWidth && !isIpad()) {
                var i = 0;
                $('.panel .sublevel').each(function () {
                    i++;
                    hrefValue[i] = $(this).attr('href');
                    $(this).attr('href', 'javascript:void(0)');
                });
            }
        }
        forTabletTouch();
        $(window).on("resize", forTabletTouch);
        $(document).on('click', '.panel a.sublevel', function (event) {
            if (typeof window.ontouchstart !== 'undefined' && $(window).width() > 1023 && $(window).width() <= 1280 && window.innerHeight < window.innerWidth && !isIpad()) {
                if (!$(this).parent().hasClass('firstClick')) {
                    $('.panel a.sublevel').each(function () {
                        $(this).parent().removeClass('firstClick');
                    });
                    $(this).parent().addClass('firstClick');
                    $('.panel-backdrop').show();
    
                } else {
                    var i = 0;
                    $('.panel a.sublevel').each(function () {
                        i++;
                        $(this).attr('href', hrefValue[i]);
                    });
                    $(this).parent().removeClass('firstClick');
                    $('.panel-backdrop').hide();
                }
            }
        });
    
        $(document).click(function (event) {
            if (!($(event.target).hasClass('sublevel')) && !($(event.target).parents().hasClass('sub-menu'))) {
                var i = 0;
                $('.panel a.sublevel').each(function () {
                    i++;
                    $(this).attr('href', hrefValue[i]);
                });
                $('.panel a.sublevel').parent().removeClass('firstClick');
                $('.panel-backdrop').hide();
                hrefValue = [];
                forTabletTouch();
            }
        });
});

var header, offsetHeight, headerHeight, stickyHeader, cookieHeight;
function cookieInfoHide() {
            var newHeaderHeight;
            var totalHeaderHeight = $('#header header').height();
            $('.search-box').css("top", totalHeaderHeight);
            $('body').css("padding-top", totalHeaderHeight);
            $('.is_stuck').css("top", totalHeaderHeight);
            $(".cookies-msg").remove();
            $('#header header').css("top", "0px");
            var navbarHeight = $(".navbar-brand").outerHeight();
            $('.minicart-zoom .modal-dialog').css("top", navbarHeight);
        }